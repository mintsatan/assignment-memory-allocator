#include <stdio.h>
#include <string.h>
#include "mem_internals.h"
#include "mem.h"
#include "mem_debug.h"

void init_test(
  char * const test_name,
  size_t const block_length
) {
  printf("Test - %s; ", test_name);
  printf("Block length - %zu; ", block_length);

  uint8_t * test_block = (uint8_t *)_malloc(block_length);

  printf("Filling block with values; ");
  for (size_t i = 0; i < block_length; i++)
  {
    test_block[i] = i % 255;
  }

  printf("Freeing a block\n");
  _free(test_block);
}

int main()
{
  void *heap = heap_init(20000);

  init_test("#1", 10);
  debug_heap(stdout, heap);

  init_test("#2", 30);
  debug_heap(stdout, heap);

  init_test("#3", 2000);
  debug_heap(stdout, heap);

  return 0;
}
